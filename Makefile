CC = gcc
CLFAGS = -O0
BUILDDIR = build
SRCDIR = solution/src

ASM = nasm
ASMFLAGS = -felf64

all : $(BUILDDIR)/main.o $(BUILDDIR)/image.o $(BUILDDIR)/bmp_reader.o $(BUILDDIR)/bmp_writer.o $(BUILDDIR)/io.o $(BUILDDIR)/filter.o $(BUILDDIR)/sepia.o $(BUILDDIR)/sepia_impl.o $(BUILDDIR)/bmp.o
	$(CC) -o $(CFLAGS) $(BUILDDIR)/main $^

$(BUILDDIR)/%.o: $(SRCDIR)/%.c
	mkdir -p $(BUILDDIR)
	$(CC) -c $(CFLAGS) $@ $<

$(BUILDDIR)/%.o: $(SRCDIR)/%.asm
	mkdir -p $(BUILDDIR)
	nasm -felf64 -o $@ $<

clean:
	rm -rf $(BUILDDIR)

.phony: clean