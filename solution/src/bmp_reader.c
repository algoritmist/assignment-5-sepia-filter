//
// Created by viacheslav on 11.11.22.
//
#include "../include/bmp_reader.h"
#include "../include/bmp.h"

#define FORMAT_TYPE 0x4d42

static enum read_status read_header(FILE **in, struct bmp_header *const header) {
    if (fread(header, sizeof(struct bmp_header), 1, *in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (header->bfType != FORMAT_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    return READ_OK;
}

static enum read_status read_image(FILE **in, struct image *const img) {
    uint8_t padding = get_padding(img->width);
    size_t width = img->width;

    for (size_t row = 0; row < img->height; ++row) {
        size_t read_pixels = fread(img->data + row * width,
                                   sizeof(struct pixel),
                                   img->width, *in);
        if (read_pixels != width) {
            return READ_INVALID_BITS;
        }
        if (fseek(*in, padding, SEEK_CUR)) {
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum read_status from_bmp(FILE **in, struct image *const img) {
    struct bmp_header header = {0};
    enum read_status status = read_header(in, &header);

    if (status != READ_OK) {
        return status;
    }

    alloc_image(img, header.biWidth, header.biHeight);
    return read_image(in, img);
}
