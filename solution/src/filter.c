//
// Created by viacheslav on 21.01.23.
//
#include "../include/filter.h"
#include "../include/image.h"

int filter_image(filter filter, struct image *const src, struct image *const dest) {
    return (filter)(src, dest);
}