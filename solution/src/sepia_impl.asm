%macro clear_xmms 0
    pxor xmm0, xmm0
    pxor xmm1, xmm1
    pxor xmm2, xmm2
    pxor xmm3, xmm3
    pxor xmm4, xmm4
    pxor xmm5, xmm5
%endmacro


; 4 pixels at rdi
%macro load_pixels 0
    ;xmm1 = b1 b1 b1 b2 | b2 b2 b3 b3 | b3 b4 b4 b4
    ;xmm2 = g1 g1 g1 g2 | g2 g2 g3 g3 | g3 g4 g4 g4
    ;xmm3 = r1 r1 r1 r2 | r2 r2 r3 r3 | r3 r4 r4 r4
    %assign i 0
    %rep 4
    pinsrb xmm0, [rdi + 2],     i       ; b[i][0]
    pinsrb xmm0, [rdi + 2],     i + 4   ; b[i][1]
    pinsrb xmm0, [rdi + 2],     i + 8   ; b[i][2]

    pinsrb xmm1, [rdi + 1],     i       ; g[i][0]
    pinsrb xmm1, [rdi + 1],     i + 4   ; g[i][1]
    pinsrb xmm1, [rdi + 1],     i + 8   ; g[i][2]

    pinsrb xmm2, [rdi + 0],     i       ; r[i][0]
    pinsrb xmm2, [rdi + 0],     i + 4   ; r[i][1]
    pinsrb xmm2, [rdi + 0],     i + 8   ; r[i][2]
    add rdi, 3
    %assign i i+12
    %endrep

%endmacro

%macro load_coefs 0

    ;xmm4 = c11 c12 c13 c11 | c21 c31 c11 c21 | c31 c11 c21 c31
    ;xmm5 = c21 c22 c32 c12 | c22 c32 c12 c22 | c32 c12 c22 c32
    ;xmm6 = c13 c23 c33 c13 | c23 c33 c13 c23 | c33 c13 c23 c33

    mov r8, blue_sepia
    movups xmm3, [r8]
    mov r8, green_sepia
    movups xmm4, [r8]
    mov r8, red_sepia
    movups xmm5, [r8]
%endmacro

%macro filter_pixels 0
    ; convert values to doubles
    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2
    ; xmm0 = xmm1 * xmm4 + xmm2 * xmm5 + xmm3 * xmm6
    ; multiply
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    ; finally add
    addps xmm0, xmm1
    addps xmm0, xmm2

    ; back to ints
    cvtps2dq xmm0, xmm0
    packssdw xmm0, xmm0
    packuswb xmm0, xmm0
    ; take modulo
    ;mov r8, max_values
    ;pminsd xmm0, [r8]
%endmacro

%macro return_results 0
    %assign i 0
    %rep 4
    pextrb [rsi],   xmm0,   i
    pextrb [rsi+1], xmm0,   i + 4
    pextrb [rsi+2], xmm0,   i + 8
    add rsi, 3
    %assign i i + 12
    %endrep
%endmacro

section .rodata
align 64
red_sepia   dd 0.272, 0.393, 0.349, 0.272, 0.393, 0.349, 0.272, 0.393, 0.349, 0.272, 0.393, 0.349
align 64
green_sepia dd 0.534, 0.686, 0.769, 0.534, 0.686, 0.769, 0.534, 0.686, 0.769, 0.534, 0.686, 0.769
align 64
blue_sepia  dd 0.131, 0.168, 0.189, 0.131, 0.168, 0.189, 0.131, 0.168, 0.189, 0.131, 0.168, 0.189

;align 64
;max_values:       dd 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF

section .text
global filter_asm
filter_asm:
    clear_xmms
    load_pixels
    load_coefs
    filter_pixels
    return_results
