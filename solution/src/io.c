//
// Created by viacheslav on 25.10.22.
//

#include "../include/reader.h"
#include "../include/writer.h"

enum read_status read_image(const fmt_reader reader, FILE **in, struct image * img) {
    return (reader)(in, img);
}

enum write_status write_image(const fmt_writer writer, FILE **out, struct image * img) {
    return (writer)(out, img);
}
