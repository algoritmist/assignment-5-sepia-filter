//
// Created by viacheslav on 21.01.23.
//

#include "../include/sepia.h"
#include "../include/image.h"

static uint8_t color(struct pixel pixel, double rWeight, double gWeight, double bWeight) {
    double value = pixel.r * rWeight + pixel.g * gWeight + pixel.b * bWeight;
    return value < 255 ? (uint8_t) value : 255;
}

static struct pixel do_filter(struct pixel const src) {
    struct pixel pixel = {0};
    pixel.r = color(src, .393, .769, .189);
    pixel.g = color(src, .349, .686, .168);
    pixel.b = color(src, .272, .534, .131);
    return pixel;
}

int sepia(struct image *const src, struct image *const dest) {
    if (!alloc_image(dest, src->width, src->height)) {
        return 0;
    }
    for (size_t row = 0; row < src->height; ++row) {
        for (size_t col = 0; col < src->width; ++col) {
            struct pixel original = get_pixel(src, row, col);
            set_pixel(dest, row, col, do_filter(original));
        }
    }
    return 1;
}

extern void filter_asm(struct pixel[static 4], struct pixel *restrict);

int sepia_asm(struct image *const src, struct image *const dest) {
    if (!alloc_image(dest, src->width, src->height)) {
        return 0;
    }
    for (size_t i = 0; 4 * i < get_size(src); ++i) {
        filter_asm(src->data + 4 * i, dest->data + 4 * i);
    }
    for (size_t i = get_size(src) - get_size(src) % 4; i < get_size(src); ++i) {
        dest->data[i] = do_filter(src->data[i]);
    }
    return 1;
}