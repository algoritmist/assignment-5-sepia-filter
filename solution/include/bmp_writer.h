//
// Created by viacheslav on 12.11.22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_WRITER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_WRITER_H

#include "image.h"
#include "status.h"
#include <stdio.h>

enum write_status to_bmp(FILE **out, struct image *img);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_BMP_WRITER_H
