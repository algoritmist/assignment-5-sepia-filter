//
// Created by viacheslav on 21.01.23.
//

#ifndef ASSIGNMENT_5_SEPIA_FILTER_SEPIA_H
#define ASSIGNMENT_5_SEPIA_FILTER_SEPIA_H

#include "filter.h"

int sepia(struct image *src, struct image *dest);

int sepia_asm(struct image *src, struct image *dest);

#endif //ASSIGNMENT_5_SEPIA_FILTER_SEPIA_H
