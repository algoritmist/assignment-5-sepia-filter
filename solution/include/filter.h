//
// Created by viacheslav on 21.01.23.
//

#ifndef ASSIGNMENT_5_SEPIA_FILTER_FILTER_H
#define ASSIGNMENT_5_SEPIA_FILTER_FILTER_H

#include "image.h"

typedef int (*filter)(struct image *const src, struct image *const dest);

int filter_image(filter filter, struct image *src, struct image *dest);

#endif //ASSIGNMENT_5_SEPIA_FILTER_FILTER_H
