//
// Created by viacheslav on 25.10.22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_READER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_READER_H

#include "image.h"
#include "status.h"
#include <stdio.h>


typedef enum read_status (*fmt_reader)(FILE **, struct image *const);

enum read_status read_image(fmt_reader reader, FILE **in, struct image *img);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_READER_H
