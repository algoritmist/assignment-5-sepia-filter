//
// Created by viacheslav on 14.10.22.
//


#ifndef ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H

#include "pixel.h"
#include <stdlib.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

void free_img(struct image *img);

int alloc_image(struct image *image, uint64_t width, uint64_t height);

struct pixel get_pixel(struct image *src, size_t row, size_t col);

void set_pixel(struct image *dest, size_t row, size_t col, struct pixel value);

 size_t get_size(struct image *img);

 size_t get_full_size(struct image *img);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_IMAGE_H
